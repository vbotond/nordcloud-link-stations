package org.nordcloud.linkstations.util;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * Not running basic getter tests. Focusing on the functionality. Testing if the implementation does link station finding correct:
 * <ul>
 * <li>between station 1 and 2 of 3*reach distance from one another station 1 is chosen for a close spot for it</li>
 * <li>between station 1 and 2 of 3*reach distance from one another no station is found for a spot in the middle</li>
 * <li>between station 1 and 2 of 3*reach distance from one another station 2 is chosen for a close spot for it</li>
 * </ul>
 * For stream based implementation it is also imperative to test if it provides the same results as the basic implementation as a base. The necessity is not coming from the
 * functional unsoundness of the solution but the multithreading nature of it. Multithreading solutions shall always be hammer tested to be kept honest... or more like as honest
 * as possible.
 * <p>
 * <b>Mind however that with multiple threads in place there is almost never any chance for full proof!</b>
 */
@Test
public class StreamLinkStationNetworkTest {
    private static final double EPSILON = 0.000001;

    private static final double COORDINATE_0 = 0.0;
    private static final double REACH_10 = 10.0;

    private static final int ROUNDS = 1000;
    private static final int LINK_STATIONS = 30;
    private static final double RANGE = 100.0;
    private static final double REACH_RANGE = 15.0;
    private static final int LOCATIONS = 100;


    public void scenariosForTwoStationsWork() {
        // given
        final LinkStation linkStation1 = new LinkStation(new Location(COORDINATE_0, COORDINATE_0), REACH_10);
        final LinkStation linkStation2 = new LinkStation(new Location(COORDINATE_0 + 3.0 * REACH_10, COORDINATE_0), REACH_10);

        // when
        final Set<LinkStation> linkStations = new HashSet<>(Arrays.asList(linkStation1, linkStation2));
        final LinkStationNetwork linkStationNetwork = new StreamLinkStationNetwork(linkStations);
        final Location withinReachForStation1 = new Location(COORDINATE_0 + 0.5 * REACH_10, COORDINATE_0);
        final Location withoutReach = new Location(COORDINATE_0 + 1.5 * REACH_10, COORDINATE_0);
        final Location withinReachForStation2 = new Location(COORDINATE_0 + 2.5 * REACH_10, COORDINATE_0);

        // then
        assertEquals(linkStation1, linkStationNetwork.findBestFor(withinReachForStation1, EPSILON).getLinkStation());
        assertNull(linkStationNetwork.findBestFor(withoutReach, EPSILON));
        assertEquals(linkStation2, linkStationNetwork.findBestFor(withinReachForStation2, EPSILON).getLinkStation());
    }

    public void hammerTestAgainstBasicImplementation() {
        final Random random = new Random();

        for (int r = 0; r < ROUNDS; r++) {
            // given
            final Set<LinkStation> linkStations = new HashSet<>();
            for (int ls = 0; ls < LINK_STATIONS; ls++) {
                linkStations.add(LinkStation.randomLinkStation(RANGE, RANGE, REACH_RANGE));
            }
            final LinkStationNetwork linkStationNetwork1 = new BasicLinkStationNetwork(linkStations);
            final LinkStationNetwork linkStationNetwork2 = new StreamLinkStationNetwork(linkStations);

            // when
            final List<Location> locations = new ArrayList<>();
            for (int l = 0; l < LOCATIONS; l++) {
                locations.add(Location.randomLocation(RANGE, RANGE));
            }

            // then
            for (Location l : locations) {
                final PowerCalculation powerCalculation1 = linkStationNetwork1.findBestFor(l, EPSILON);
                final PowerCalculation powerCalculation2 = linkStationNetwork2.findBestFor(l, EPSILON);

                assertTrue((powerCalculation1 == null && powerCalculation2 == null) ||
                           powerCalculation1.getLinkStation().equals(powerCalculation2.getLinkStation()));
            }
        }
    }
}
