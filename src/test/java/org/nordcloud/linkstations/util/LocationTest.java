package org.nordcloud.linkstations.util;

import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;

/**
 * Not running basic getter tests. Focusing on the functionality. Testing if the implementation does distance calculations right:
 * <ul>
 * <li>unit vectors have 1.0 length (endpoints show 1.0 distance from one another)</li>
 * <li>distance calculation is reflective (a point has 0.0 distance from itself)</li>
 * <li>distance calculation is commutative (point A is as far from point B as point B is from point A)</li>
 * <li>distance calculation works according to formula sqrt(dx^2 + dy^2)</li>
 * </ul>
 */
@Test
public class LocationTest {
    private static final double EPSILON = 0.000001;

    private static final double COORDINATE_0 = 0.0;
    private static final double COORDINATE_1 = 1.0;

    private static final Random RANDOM = new Random();

    private static final double RANDOM_X_COORDINATE_1 = RANDOM.nextDouble();
    private static final double RANDOM_Y_COORDINATE_1 = RANDOM.nextDouble();
    private static final double RANDOM_X_COORDINATE_2 = RANDOM.nextDouble();
    private static final double RANDOM_Y_COORDINATE_2 = RANDOM.nextDouble();


    public void unitPointDistancesAreOne() {
        // given
        final Location origin = new Location(COORDINATE_0, COORDINATE_0);

        // when
        final Location unitToNorth = new Location(COORDINATE_0, COORDINATE_1);
        final Location unitToEast = new Location(COORDINATE_1, COORDINATE_0);
        final Location unitToSouth = new Location(COORDINATE_0, -COORDINATE_1);
        final Location unitToWest = new Location(-COORDINATE_1, COORDINATE_0);

        // then
        assertEquals(1.0, origin.distanceFrom(unitToNorth), EPSILON);
        assertEquals(1.0, origin.distanceFrom(unitToEast), EPSILON);
        assertEquals(1.0, origin.distanceFrom(unitToSouth), EPSILON);
        assertEquals(1.0, origin.distanceFrom(unitToWest), EPSILON);
    }

    public void distanceCalculationIsReflexive() {
        // given
        final Location origin = new Location(RANDOM_X_COORDINATE_1, RANDOM_Y_COORDINATE_1);

        // when

        // then
        assertEquals(0.0, origin.distanceFrom(origin), EPSILON);
    }

    public void distanceCalculationIsCommutative() {
        // given
        final Location location1 = new Location(RANDOM_X_COORDINATE_1, RANDOM_Y_COORDINATE_1);
        final Location location2 = new Location(RANDOM_X_COORDINATE_2, RANDOM_Y_COORDINATE_2);

        // when

        // then
        assertEquals(location1.distanceFrom(location2), location2.distanceFrom(location1), EPSILON);
    }

    public void distanceCalculationIsDirectionAgnostic() {
        // given
        final Location origo = new Location(COORDINATE_0, COORDINATE_0);
        final Location location = new Location(RANDOM_X_COORDINATE_1, RANDOM_Y_COORDINATE_1);

        // when

        // then
        assertEquals(Math.sqrt(Math.pow(location.getX() - origo.getX(), 2) + Math.pow(location.getY() - origo.getY(), 2)), origo.distanceFrom(location), EPSILON);
    }
}
