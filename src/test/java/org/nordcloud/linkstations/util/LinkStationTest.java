package org.nordcloud.linkstations.util;

import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;

/**
 * Not running basic getter tests. Focusing on the functionality. Testing if the implementation does power calculations right:
 * <ul>
 * <li>power output is 0 form out of reach</li>
 * <li>power output is 0 form reach perimeter (reach - distance = 0)</li>
 * <li>power output equals to (reach)^2 at the link station location</li>
 * <li>power output within perimeter us (reach - distance)^2</li>
 * </ul>
 */
@Test
public class LinkStationTest {
    private static final double EPSILON = 0.000001;


    private static final double COORDINATE_0 = 0.0;
    private static final double REACH_10 = 10.0;


    public void noPowerFromOutOfReach() {
        // given
        final Location linkStationLocation = new Location(COORDINATE_0, COORDINATE_0);
        final LinkStation linkStation = new LinkStation(linkStationLocation, REACH_10);

        // when
        final Location outOfReachLocation = new Location(COORDINATE_0 + 2 * REACH_10, COORDINATE_0);

        // then
        assertEquals(0.0, linkStation.calculatePowerAt(outOfReachLocation), EPSILON);
    }

    public void noPowerFromPerimeter() {
        // given
        final Location linkStationLocation = new Location(COORDINATE_0, COORDINATE_0);
        final LinkStation linkStation = new LinkStation(linkStationLocation, REACH_10);

        // when
        final Location perimeterLocation = new Location(COORDINATE_0 + REACH_10, COORDINATE_0);

        // then
        assertEquals(0.0, linkStation.calculatePowerAt(perimeterLocation), EPSILON);
    }

    public void powerFromUpCloseIsSquareReach() {
        // given
        final Location linkStationLocation = new Location(COORDINATE_0, COORDINATE_0);
        final LinkStation linkStation = new LinkStation(linkStationLocation, REACH_10);

        // when

        // then
        assertEquals(Math.pow(REACH_10, 2), linkStation.calculatePowerAt(linkStation.getLocation()), EPSILON);
    }

    public void powerLossIsGradualWithinPerimeter() {
        // given
        final Location linkStationLocation = new Location(COORDINATE_0, COORDINATE_0);
        final LinkStation linkStation = new LinkStation(linkStationLocation, REACH_10);

        // when
        final Random random = new Random();
        final double offset = 0.95 * linkStation.getReach() * random.nextDouble(); // random offset within 95% 'reach'
        final Location deviceLocation = new Location(COORDINATE_0 + offset, COORDINATE_0);

        // then
        assertEquals(Math.pow(linkStation.getReach() - offset, 2), linkStation.calculatePowerAt(deviceLocation), EPSILON);
    }
}
