package org.nordcloud.linkstations.util;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Not running basic getter tests. Focusing on the functionality. Testing if the implementation does link station finding correct:
 * <ul>
 * <li>between station 1 and 2 of 3*reach distance from one another station 1 is chosen for a close spot for it</li>
 * <li>between station 1 and 2 of 3*reach distance from one another no station is found for a spot in the middle</li>
 * <li>between station 1 and 2 of 3*reach distance from one another station 2 is chosen for a close spot for it</li>
 * </ul>
 */
@Test
public class BasicLinkStationNetworkTest {
    private static final double EPSILON = 0.000001;

    private static final double COORDINATE_0 = 0.0;
    private static final double REACH_10 = 10.0;


    public void scenariosForTwoStationsWork() {
        // given
        final LinkStation linkStation1 = new LinkStation(new Location(COORDINATE_0, COORDINATE_0), REACH_10);
        final LinkStation linkStation2 = new LinkStation(new Location(COORDINATE_0 + 3.0 * REACH_10, COORDINATE_0), REACH_10);

        // when
        final Set<LinkStation> linkStations = new HashSet<>(Arrays.asList(linkStation1, linkStation2));
        final LinkStationNetwork linkStationNetwork = new BasicLinkStationNetwork(linkStations);
        final Location withinReachForStation1 = new Location(COORDINATE_0 + 0.5 * REACH_10, COORDINATE_0);
        final Location withoutReach = new Location(COORDINATE_0 + 1.5 * REACH_10, COORDINATE_0);
        final Location withinReachForStation2 = new Location(COORDINATE_0 + 2.5 * REACH_10, COORDINATE_0);

        // then
        assertEquals(linkStation1, linkStationNetwork.findBestFor(withinReachForStation1, EPSILON).getLinkStation());
        assertNull(linkStationNetwork.findBestFor(withoutReach, EPSILON));
        assertEquals(linkStation2, linkStationNetwork.findBestFor(withinReachForStation2, EPSILON).getLinkStation());
    }
}
