package org.nordcloud.linkstations.util;

import java.util.Random;

/**
 * Represents a link station with a location and a reach parameter. Also provides method to calculate the power the station generates at a given location using the provided
 * formula.
 * <pre>
 *     power = (reach - distance-from-supply) ^ 2     // assuming location is within reach (distance is less then or equal to reach)
 *     power = 0                                      // when location is outside reach (distance greater than reach)
 * </pre>
 *
 * @author Botond Vasarhelyi
 */
public class LinkStation {
    private static final String TO_STRING_FORMAT = "[%.2f, %.2f, %.2f]";
    private static final Random RANDOM = new Random();

    private final Location location;
    private final double reach;

    /**
     * Returns a new random link station using the provided <code>coordinate</code> and <code>reach ranges</code>.
     *
     * @param xRange
     *     the range for x coordinate
     * @param yRange
     *     the range for y coordinate
     * @param reachRange
     *     the range for the reach of the link station
     *
     * @return the new random location
     */
    public static LinkStation randomLinkStation(final double xRange, final double yRange, final double reachRange) {
        final Location location = Location.randomLocation(xRange, yRange);
        final double reach = reachRange * RANDOM.nextDouble();
        return new LinkStation(location, reach);
    }

    /**
     * Creates a new instance with the given <code>location</code> and the specified <code>reach</code>.
     *
     * @param location
     *     the location of the link station
     * @param reach
     *     the reach length of the link station
     */
    public LinkStation(final Location location, final double reach) {
        this.location = location;
        this.reach = reach;
    }

    /**
     * Returns the location of the link station.
     *
     * @return the location of the link station
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Returns the reach distance of the link station.
     *
     * @return the reach distance of the link station
     */
    public double getReach() {
        return reach;
    }

    /**
     * Returns the calculated power the link station is able to provide at a specified <code>deviceLocation</code>.
     * <pre>
     *     power = (reach - distance-from-supply) ^ 2     // assuming location is within reach (distance is less then or equal to reach)
     *     power = 0                                      // when location is outside reach (distance greater than reach)
     * </pre>
     *
     * @param deviceLocation
     *     the location of the device to calculate power for
     *
     * @return the power the device at the given location shall have
     */
    public double calculatePowerAt(final Location deviceLocation) {
        final double distance = deviceLocation.distanceFrom(location);

        return distance > reach ? 0.0 : Math.pow(reach - distance, 2);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return String.format(TO_STRING_FORMAT, location.getX(), location.getY(), reach);
    }
}
