package org.nordcloud.linkstations.util;

import java.util.Set;

/**
 * Abstract link station network that deals with all so that specific implementations can concentrate on the search algorithm.
 *
 * @author Botond Vasarhelyi
 */
public abstract class AbstractLinkStationNetwork implements LinkStationNetwork {
    private Set<LinkStation> linkStations;

    /**
     * Creates a new instance with the given <code>link stations</code>.
     *
     * @param linkStations
     *     the link stations to calculate with
     */
    public AbstractLinkStationNetwork(final Set<LinkStation> linkStations) {
        this.linkStations = linkStations;
    }

    /**
     * Returns the link stations the network holds.
     *
     * @return the link stations within the network
     */
    public Set<LinkStation> getLinkStations() {
        return linkStations;
    }
}
