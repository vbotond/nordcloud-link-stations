package org.nordcloud.linkstations.util;

/**
 * Represents a power calculation holding all parameters as well as the result.
 *
 * @author Botond Vasarhelyi
 */
public class PowerCalculation {
    private final LinkStation linkStation;
    private final Location deviceLocation;
    private final double power;

    /**
     * Creates a new instance with the given <code>link station</code>, <code>device location</code> and <code>power calculation result</code>.
     *
     * @param linkStation
     *     the link station the calculation was made for
     * @param deviceLocation
     *     the location of the device the calculation was made for
     * @param power
     *     the power calculated
     */
    public PowerCalculation(final LinkStation linkStation, final Location deviceLocation, final double power) {
        this.linkStation = linkStation;
        this.deviceLocation = deviceLocation;
        this.power = power;
    }

    /**
     * Returns the link station the calculation was done for.
     *
     * @return the link station
     */
    public LinkStation getLinkStation() {
        return linkStation;
    }

    /**
     * Returns the location of the device the calculation was done for.
     *
     * @return the device location
     */
    public Location getDeviceLocation() {
        return deviceLocation;
    }

    /**
     * Returns the power that was calculated
     *
     * @return the calculated power
     */
    public double getPower() {
        return power;
    }
}
