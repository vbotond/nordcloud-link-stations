package org.nordcloud.linkstations.util;

/**
 * Defines a link station network that is holding a set of link stations as well as means to find the strongest source for a given location.
 *
 * @author Botond Vasarhelyi
 */
public interface LinkStationNetwork {
    /**
     * Finds the best link station (the one that provides the highest amount of power) for the given <code>location</code>. Returns <code>null</code> in case there is none. Uses
     * the specified <code>epsilon</code> value for error margin.
     * <p>
     * Epsilon is needed since any floating point real number representation is, by nature, not precise. Namely 1.0 cannot be expected to be really 1.0 in representation. This
     * may lead to anomalies then 1.0 is not equal to 1.0. So comparisons are mostly done using an epsilon value for margin of error.
     *
     * @param location
     *     the location to check power levels at
     * @param epsilon
     *     the error margin value
     *
     * @return the calculation result for the best link station for the device at the given location or <code>null</code> in case no station is within reach
     */
    PowerCalculation findBestFor(final Location location, final double epsilon);
}
