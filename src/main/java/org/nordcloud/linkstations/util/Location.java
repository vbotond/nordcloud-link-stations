package org.nordcloud.linkstations.util;

import java.util.Random;

/**
 * Represents location by coordinates. Could have used AWT or JFX classes but to have a clean solution decided to do it over. The class also provides means to calculate
 * the distance between to locations using the standard coordinate geometry rules.
 *
 * @author Botond Vasarhelyi
 * @see <a href="https://www.mathopenref.com/coorddist.html">coodrinate distance calculation</a>
 */
public class Location {
    private static final String TO_STRING_FORMAT = "[%.2f, %.2f]";
    private static final Random RANDOM = new Random();

    private final double x;
    private final double y;

    /**
     * Returns a new random location within the provided <code>coordinate ranges</code>.
     *
     * @param xRange
     *     the range for x coordinate
     * @param yRange
     *     the range for y coordinate
     *
     * @return the new random location
     */
    public static Location randomLocation(final double xRange, final double yRange) {
        final double x = xRange * RANDOM.nextDouble();
        final double y = yRange * RANDOM.nextDouble();
        return new Location(x, y);
    }

    /**
     * Creates a new instance with the specified <code>coordinates</code>.
     *
     * @param x
     *     the x coordinate to use
     * @param y
     *     the y coordinate to use
     */
    public Location(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the x coordinate.
     *
     * @return the x coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the y coordinate.
     *
     * @return the y coordinate
     */
    public double getY() {
        return y;
    }

    /**
     * Returns the distance from a given <code>location</code> using coordinate distance calculation.
     * <pre>
     *     distance = sqrt ( (x2 - x1)^2 + (y2 - y1)^2)
     * </pre>
     *
     * @param location
     *     the location to calculate the distance from
     *
     * @return the scalar distance from the calculation
     *
     * @see <a href="https://www.mathopenref.com/coorddist.html">coodrinate distance calculation</a>
     */
    public double distanceFrom(final Location location) {
        return Math.sqrt(Math.pow(x - location.getX(), 2) + Math.pow(y - location.getY(), 2));
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return String.format(TO_STRING_FORMAT, x, y);
    }
}
