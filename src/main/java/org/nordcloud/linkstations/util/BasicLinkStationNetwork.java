package org.nordcloud.linkstations.util;

import java.util.Set;

/**
 * Basic link station network implementation.
 * <p>
 * Find method simply runs through all the link stations in the network calculating the power it has available at the location in question. It is entirely sequential and works
 * by the book. No shortcuts, no tricks, no speed enhancements.
 *
 * @author Botond Vasarhelyi
 */
public class BasicLinkStationNetwork extends AbstractLinkStationNetwork {
    /** {@inheritDoc} */
    public BasicLinkStationNetwork(final Set<LinkStation> linkStations) {
        super(linkStations);
    }

    /** {@inheritDoc} */
    @Override
    public PowerCalculation findBestFor(final Location location, final double epsilon) {
        PowerCalculation result = null;
        double maximumPower = 0.0;
        for (LinkStation ls : getLinkStations()) {
            final double power = ls.calculatePowerAt(location);
            if (power > maximumPower) {
                maximumPower = power;
                result = new PowerCalculation(ls, location, power);
            }
        }
        return maximumPower > epsilon ? result : null;
    }
}
