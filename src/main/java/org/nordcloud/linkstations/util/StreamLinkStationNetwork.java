package org.nordcloud.linkstations.util;

import java.util.Optional;
import java.util.Set;

/**
 * Stream based link station network implementation.
 * <p>
 * Find method works with Java 8 streams. More specifically parallel streams. This way the multithreading processing of the link station set is handled by java internally.
 *
 * @author Botond Vasarhelyi
 */
public class StreamLinkStationNetwork extends AbstractLinkStationNetwork {
    /** {@inheritDoc} */
    public StreamLinkStationNetwork(final Set<LinkStation> linkStations) {
        super(linkStations);
    }

    /**
     * Finds the best link station (the one that provides the highest amount of power) for the given <code>location</code>. Returns <code>null</code> in case there is none. Uses
     * the specified <code>epsilon</code> value for error margin.
     * <p>
     * Epsilon is needed since any floating point real number representation is, by nature, not precise. Namely 1.0 cannot be expected to be really 1.0 in representation. This
     * may lead to anomalies then 1.0 is not equal to 1.0. So comparisons are mostly done using an epsilon value for margin of error.
     * <p>
     * The steps to solve the problem are:
     * <ol>
     * <li>map to all link station the related power calculation</li>
     * <li>filter out the ones with 0 (mind the epsilon margin) as those are out of reach</li>
     * <li>get the one with the highest amount of power output</li>
     * </ol>
     *
     * @param location
     *     the location to check power levels at
     * @param epsilon
     *     the error margin value
     *
     * @return the calculation result for the best link station for the device at the given location or <code>null</code> in case no station is within reach
     */
    @Override
    public PowerCalculation findBestFor(final Location location, final double epsilon) {
        final Optional<PowerCalculation> result =
            getLinkStations()
                .parallelStream()
                .map(ls -> {
                    final PowerCalculation powerCalculation = new PowerCalculation(ls, location, ls.calculatePowerAt(location));
                    return powerCalculation;
                })
                .filter(ls -> ls.getPower() > epsilon)
                .max((ls1, ls2) -> Double.valueOf(ls1.getPower()).compareTo(Double.valueOf(ls2.getPower())));

        return result.isPresent() ? result.get() : null;
    }
}
