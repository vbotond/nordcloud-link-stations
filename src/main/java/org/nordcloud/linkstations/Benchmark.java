package org.nordcloud.linkstations;

import org.nordcloud.linkstations.util.BasicLinkStationNetwork;
import org.nordcloud.linkstations.util.LinkStation;
import org.nordcloud.linkstations.util.LinkStationNetwork;
import org.nordcloud.linkstations.util.Location;
import org.nordcloud.linkstations.util.StreamLinkStationNetwork;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Main application to benchmark the different 'link station network' implementations.
 *
 * @author Botond Vasarhelyi
 */
@Command(
    name = "org.nordcloud.linkstations.Benchmark",
    showDefaultValues = true,
    sortOptions = false,
    helpCommand = true)
public class Benchmark implements Callable<Void> {
    @Option(names = { "-x", "--field-range-x" }, description = "the X range of the link station network field")
    private double fieldRangeX = 100.0;
    @Option(names = { "-y", "--field-range-y" }, description = "the Y range of the link station network field")
    private double fieldRangeY = 100.0;
    @Option(names = { "-r", "--reach-range" }, description = "the range for link station reach parameters")
    private double reachRange = 15.0;
    @Option(names = { "-e", "--epsilon" }, description = "the error margin for floating point comparisons")
    private double epsilon = 0.000001;

    @Option(names = { "-lsa", "--link-station-amounts" }, split = ",", description = "the list of link station amounts to run benchmark with")
    private int[] linkStationAmounts = new int[]{ 1, 10, 100, 1000, 10000, 100000 };
    @Option(names = { "-la", "--location-amount" }, description = "the amount of random locations to generate at each round")
    private int locationAmount = 1000;


    /**
     * Main entry point of the application receiving all the command line arguments.
     *
     * @param arguments
     *     the command line arguments
     */
    public static void main(final String[] arguments) {
        Locale.setDefault(new Locale("en", "US")); // to force '.' as decimal separator
        CommandLine.call(new Benchmark(), arguments);
    }

    @Override
    public Void call() throws Exception {
        benchmark();
        return null;
    }

    private void benchmark() {
        System.out.printf("field range: [%.2f, %.2f]\n", fieldRangeX, fieldRangeY);

        for (int la : linkStationAmounts) {
            final Set<LinkStation> linkStations = new HashSet<>();
            for (int ls = 0; ls < la; ls++) {
                linkStations.add(LinkStation.randomLinkStation(fieldRangeX, fieldRangeY, reachRange));
            }

            final LinkStationNetwork basicLinkStationNetwork = new BasicLinkStationNetwork(linkStations);
            final LinkStationNetwork streamLinkStationNetwork = new StreamLinkStationNetwork(linkStations);

            final List<Double> basicResults = new ArrayList<>();
            final List<Double> streamResults = new ArrayList<>();

            for (int l = 0; l < locationAmount; l++) {
                final Location location = Location.randomLocation(fieldRangeX, fieldRangeY);

                long start = System.nanoTime();
                basicLinkStationNetwork.findBestFor(location, epsilon);
                long end = System.nanoTime();
                basicResults.add((double) end - start);

                start = System.nanoTime();
                streamLinkStationNetwork.findBestFor(location, epsilon);
                end = System.nanoTime();
                streamResults.add((double) end - start);
            }

            printStatistics(la, basicResults, streamResults);
        }
    }

    private void printStatistics(final int linkStationAmount, final List<Double> basicResults, final List<Double> streamResults) {
        final double[] basicStats = collectStats(basicResults);
        final double[] streamStats = collectStats(streamResults);

        System.out.printf("%d stations: (stream / basic 90%%: %.2f) basic[%.2f, %.2f, %.2f, %.2f] - stream[%.2f, %.2f, %.2f, %.2f]\n",
                          linkStationAmount,
                          streamStats[3] / basicStats[3],
                          basicStats[0], basicStats[1], basicStats[2], basicStats[3],
                          streamStats[0], streamStats[1], streamStats[2], streamStats[3]);
    }

    private double[] collectStats(final List<Double> list) {
        final double[] result = new double[4];
        final long percentile = Math.round(0.90 * list.size()) - 1;

        result[0] = list.parallelStream()
                        .min(Double::compareTo).get() / 1000000;
        result[1] = list.parallelStream()
                        .max(Double::compareTo).get() / 1000000;
        result[2] = list.parallelStream()
                        .mapToDouble(Double::doubleValue)
                        .average().getAsDouble() / 1000000;
        result[3] = list.parallelStream()
                        .sorted(Double::compareTo)
                        .skip(percentile)
                        .findFirst().get() / 1000000;

        return result;
    }
}
