package org.nordcloud.linkstations;

import org.nordcloud.linkstations.util.BasicLinkStationNetwork;
import org.nordcloud.linkstations.util.LinkStation;
import org.nordcloud.linkstations.util.LinkStationNetwork;
import org.nordcloud.linkstations.util.Location;
import org.nordcloud.linkstations.util.PowerCalculation;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Implementation of the original exercise using objects.
 *
 * @author Botond Vasarhelyi
 */
@Command(
    name = "org.nordcloud.linkstations.Application",
    showDefaultValues = true,
    sortOptions = false,
    helpCommand = true)
public class Application implements Callable<Void> {
    private static final String POSITIVE_MESSAGE_FORMAT = "Best link station for %.2f,%.2f is %.2f,%.2f with power %.2f";
    private static final String NEGATIVE_MESSAGE_FORMAT = "No link station within reach for %.2f,%.2f";

    @Option(names = { "-e", "--epsilon" }, description = "the error margin for floating point comparisons")
    private double epsilon = 0.000001;

    @Option(names = { "-ls", "--link-station" }, description = "link station in 'x,y,r' format")
    private String[] linkStationDescriptors = new String[]{ "0,0,10", "20,20,5", "10,0,12" };
    private Set<LinkStation> linkStations = new HashSet<>();

    @Option(names = { "-dl", "--device-location" }, description = "device location in 'x,y' format")
    private String[] deviceLocationDescriptors = new String[]{ "0,0", "100,100", "15,10", "18,18" };
    private List<Location> deviceLocations = new ArrayList<>();


    /**
     * Main entry point of the application receiving all the command line arguments.
     *
     * @param arguments
     *     the command line arguments
     */
    public static void main(final String[] arguments) {
        Locale.setDefault(new Locale("en", "US")); // to force '.' as decimal separator
        CommandLine.call(new Application(), arguments);
    }

    @Override
    public Void call() throws Exception {
        parseLinkStations();
        parseDeviceLocations();
        demonstrate();
        return null;
    }

    private void parseLinkStations() {
        for (String lsd : linkStationDescriptors) {
            final String[] parameters = lsd.split(",");
            if (parameters.length == 3) {
                final double x = Double.valueOf(parameters[0]);
                final double y = Double.valueOf(parameters[1]);
                final double reach = Double.valueOf(parameters[2]);
                final LinkStation linkStation = new LinkStation(new Location(x, y), reach);
                System.out.printf("Link Station %s\n", linkStation);
                linkStations.add(linkStation);
            }
        }
    }

    private void parseDeviceLocations() {
        for (String dld : deviceLocationDescriptors) {
            final String[] parameters = dld.split(",");
            if (parameters.length == 2) {
                final double x = Double.valueOf(parameters[0]);
                final double y = Double.valueOf(parameters[1]);
                final Location location = new Location(x, y);
                System.out.printf("Device Location %s\n", location);
                deviceLocations.add(location);
            }
        }
    }

    private void demonstrate() {
        for (Location l : deviceLocations) {
            findBestLinkStation(linkStations, l);
        }
    }

    private void findBestLinkStation(final Set<LinkStation> linkStations, final Location deviceLocation) {
        String result;
        final LinkStationNetwork linkStationNetwork = new BasicLinkStationNetwork(linkStations);
        final PowerCalculation bestOption = linkStationNetwork.findBestFor(deviceLocation, epsilon);
        if (bestOption != null) {
            result = String.format(POSITIVE_MESSAGE_FORMAT,
                                   deviceLocation.getX(),
                                   deviceLocation.getY(),
                                   bestOption.getLinkStation().getLocation().getX(),
                                   bestOption.getLinkStation().getLocation().getY(),
                                   bestOption.getPower());
        } else {
            result = String.format(NEGATIVE_MESSAGE_FORMAT,
                                   deviceLocation.getX(),
                                   deviceLocation.getY());
        }
        // the task said 'Function should output following line'
        // there are multiple ways to interpret this but since returning string is usually a questionable practice decided to output to standard output
        System.out.println(result);
    }
}
