# LINK STATIONS #

This prject aims to give solution to the Link Station exercise sent.

Prerequisites from  target audience are:

- general coding knowledge of any language
    - apart from one method basic Java coding applied to avoid confusion
    - Java 8 streams and functional programming are used in one method to illustrate a point only
- object oriented paradigm (since the chosen language is Object Oriented)

## Quick Start ##

### Prerequisites ###

- [Java Development Kit (JDK) 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) or higher installed
- for better code analysis [IntelliJ IDEA](https://www.jetbrains.com/idea/), [eclipse](https://www.eclipse.org/), [NetBeans](https://netbeans.org/) or any other Java capable IDE
    - Gradle ensures IDE free, portable setup

No other prerequisite. The Gradle Wrapper used will take care of everything else automatically.

### Execution ###

The simplest execution can be done via Gradle Wrapper:

~~~
./gradlew run
~~~

on Linux, Unix or Mac or

~~~
gradlew run
~~~

on Windows.

This will automatically
 
- download Gradle
- compile the code
- execute all unit tests
- execute main application with no parameters

## Detailed Walkthrough ##

### Theories Used ###

- [Coordinate Distance Calculation](https://www.mathopenref.com/coorddist.html)
- [Proper Comparison of Doubles](https://howtodoinjava.com/java/basics/correctly-compare-float-double/)

### Libraries Used ###

- [TestNG](https://testng.org/doc/index.html) for unit tests
- [picocli](https://picocli.info/) for command line argument parsing

### Code Structure ###

The structure is fairly flat yet. Basically one util package has it all.

- Location class
    - represents location with coordinates
    - responsible for calculating distance from another location
- LinkStation class
    - represents a link station with location and reach
    - responsible for calculating power provided at a given location
- LinkStationNetwork interface
    - defines basic interface for link station networks
    - stipulates responsibility for finding the best link station for a given device location
- BasicLinkStationNetwork class
    - basic, direct implementation of the search algorithm
- StreamLinkStationNetwork class
    - Java 8 parallel stream based implementation of the search algorithm
    - as benchmark shows start being superior above 100k link stations only
- Application class
    - basic application that implements the given task
    - parameters allow to execute it with different link station and device location setups
- Benchmark class
    - benchmarking application that executes tests for various link station networks
    
### Build ###

For full build execute

~~~
./gradlew clean build javaDoc
~~~

on Linux, Unix or Mac or

~~~
gradlew clean build javaDoc
~~~

on Windows

This will

- clean all generated folder (clean slate)
- rebuild the entire code base
    - classes
    - jar file
- execute all unit tests
- generate JavaDoc (located in ./build/docs/javadoc - start with index.html)

### Tests ###

> Untested code can at best be hoped to be doing what it is supposed to be doing

Unit tests cannot guarantee overall correctness but their lack would highly jeopardize it. With this in mind unit tests are made to check
the correctness of all calculation algorithms. The stream based implementation is checked against the basic one as well.

### Execution ###

As discussed above

~~~
./gradlew run
~~~

executes the application with no parameters. This will print out the results for the setup provided in the exercise description.

In case any other setup one would want to test

~~~
./gradlew run --args='<arguments>'
~~~

can be used.

~~~
./gradlew run --args='--help'
~~~

prints the usage description.

### Benchmarking ###

Benchmarking can also be done via Gradle Wrapper:

~~~
./gradlew benchmark
~~~

to execute preset benchmarking or

~~~
./gradlew benchmark --args='<arguments>'
~~~

for full power.

~~~
./gradlew benchmark --args='--help'
~~~

prints out the usage description.

## Future Ideas ##

- collaboration based on [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html)
- visualization of the network and the search result would be nice
- turning the project into a reusable library
- power attribute for link stations
    - currently 'reach' is used both as operating range and as power setup
    - separating those values may result in a clearer picture
- extending the model to 3D
    - would allow to model more complex setups 
- extending the model to GPS coordinates instead of simple 2D coordinates
    - calculations are different
    - would allow to model mobile or radio network coverage
- extending the model with obstacles impacting the power calculations
    - more complex model
    - mode expressive for some modelling 
